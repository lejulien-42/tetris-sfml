// Celebrating tetris by Ley 2024

#include "./Game.hpp"

#pragma once

class Board {
	public:
		Board();
		~Board();
		void Draw(sf::RenderWindow &win);
		void Clear();
		int DoesFit(const Tetrominos::Names name, const int rot,
				const sf::Vector2<int> pos);
		void PlaceTetromino(const Tetrominos::Names name, const int rot,
				const sf::Vector2<int> pos);
		void CheckLines();
	protected:
		int isInBoard(const sf::Vector2<int> pos);
	private:
		int buffer_[10][20];
};
