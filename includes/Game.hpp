// Celebrating tetris by Ley 2024

#pragma once

// SMFL headers
#include <SFML/Graphics.hpp>
#include <SFML/System.hpp>

// Cxx headers
#include <iostream>

#include "./Tetrominos.hpp"

sf::Color getTetrominoColor(const Tetrominos::Names &name);
