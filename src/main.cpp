// Celebrating tetris by Ley 2024

#include "../includes/Game.hpp"
#include "../includes/Board.hpp"

#include <ctime>
#include <cstdlib>


void draw_tetromino(sf::RenderWindow &window, const Tetrominos::Names &name,
		const sf::Vector2<float> &pos, int anim) {
	sf::RectangleShape cell({28.f, 28.f});
	cell.setOutlineThickness(4.f);
	cell.setOutlineColor({});

	for (int y = 0; y < 4; y++) {
		for (int x = 0; x < 4; x++) {
			if (Tetrominos::shapes[name][anim][y][x] == 1) {
				cell.setPosition(x * 34.f + (16.f + pos.x * 34.f),
													 y * 34.f + (16.f + pos.y * 34.f));
				cell.setFillColor(getTetrominoColor(name));
				window.draw(cell);
			}
		}
	}
}


int main(int ac, char **av) {
	(void)ac;
	sf::Clock clock;
	sf::Clock input_clock;
	bool left_rotating = false;
	bool right_rotating = false;
	sf::RectangleShape top_hider({400.f, 12.f});
	sf::RectangleShape pred_box({155.f, 77.f});
	pred_box.setPosition(365.f, 586.f);
	sf::RenderWindow window(sf::VideoMode(520, 700), av[0]+2);
	Board board;
	top_hider.setFillColor({40, 40, 40, 255});
	float descending_delay = 500.f;
	float input_delay = 100.f;
	sf::Vector2<int> pos = {3, -2};
	int anim_id = 0;
	std::srand(std::time(nullptr));
	int pred_shape = static_cast<Tetrominos::Names>(std::rand() / ((RAND_MAX + 1u) / 7));
	int shape = static_cast<Tetrominos::Names>(std::rand() / ((RAND_MAX + 1u) / 7));

  while (window.isOpen())
  {
		auto ElapsedTime = clock.getElapsedTime();
		auto KeyElapsedTime = input_clock.getElapsedTime();

		sf::Joystick::update();
		// Joystick
		if (sf::Joystick::isConnected(0)) {
			auto buttonCount = sf::Joystick::getButtonCount(0);
			for (int i = 0; i <= buttonCount; i++) {
				std::cout << i << " : " << sf::Joystick::isButtonPressed(0, i) << std::endl;
			}
			// Moving
			if (KeyElapsedTime.asMilliseconds() > input_delay) {
				// Left & Right
				if (!(sf::Keyboard::isKeyPressed(sf::Keyboard::A) && sf::Keyboard::isKeyPressed(sf::Keyboard::D))) {
					if (sf::Keyboard::isKeyPressed(sf::Keyboard::A)) {
						if (board.DoesFit(static_cast<Tetrominos::Names>(shape), anim_id, {pos.x - 1, pos.y})) {
							pos.x--;
						}
					}
					if (sf::Keyboard::isKeyPressed(sf::Keyboard::D)) {
						if (board.DoesFit(static_cast<Tetrominos::Names>(shape), anim_id, {pos.x + 1, pos.y})) {
							pos.x++;
						}
					}
				}
				// Speeding down
				if (sf::Keyboard::isKeyPressed(sf::Keyboard::S)) {
					if (board.DoesFit(static_cast<Tetrominos::Names>(shape), anim_id, {pos.x, pos.y + 1})) {
						pos.y++;
					}
				}
				input_clock.restart();
			}
			// Rotating
			if (!(sf::Keyboard::isKeyPressed(sf::Keyboard::K) && sf::Keyboard::isKeyPressed(sf::Keyboard::L))) {
				if (sf::Keyboard::isKeyPressed(sf::Keyboard::K) && left_rotating == false) {
					int next_anim = anim_id - 1;
					if (next_anim < 0) {
						next_anim = 3;
					}
					if (board.DoesFit(static_cast<Tetrominos::Names>(shape), next_anim, {pos.x, pos.y})) {
						anim_id = next_anim;
					}
					left_rotating = true;
				}
				if (sf::Keyboard::isKeyPressed(sf::Keyboard::L) && right_rotating == false) {
					int next_anim = anim_id + 1;
					if (next_anim > 3) {
						next_anim = 0;
					}
					if (board.DoesFit(static_cast<Tetrominos::Names>(shape), next_anim, {pos.x, pos.y})) {
						anim_id = next_anim;
					}
					right_rotating = true;
				}
			}
			if (!sf::Keyboard::isKeyPressed(sf::Keyboard::K) && left_rotating == true) {
				left_rotating = false;
			}
			if (!sf::Keyboard::isKeyPressed(sf::Keyboard::L) && right_rotating == true) {
				right_rotating = false;
			}
		} else { // Keyboard
			// Moving
			if (KeyElapsedTime.asMilliseconds() > input_delay) {
				// Left & Right
				if (!(sf::Keyboard::isKeyPressed(sf::Keyboard::A) && sf::Keyboard::isKeyPressed(sf::Keyboard::D))) {
					if (sf::Keyboard::isKeyPressed(sf::Keyboard::A)) {
						if (board.DoesFit(static_cast<Tetrominos::Names>(shape), anim_id, {pos.x - 1, pos.y})) {
							pos.x--;
						}
					}
					if (sf::Keyboard::isKeyPressed(sf::Keyboard::D)) {
						if (board.DoesFit(static_cast<Tetrominos::Names>(shape), anim_id, {pos.x + 1, pos.y})) {
							pos.x++;
						}
					}
				}
				// Speeding down
				if (sf::Keyboard::isKeyPressed(sf::Keyboard::S)) {
					if (board.DoesFit(static_cast<Tetrominos::Names>(shape), anim_id, {pos.x, pos.y + 1})) {
						pos.y++;
					}
				}
				input_clock.restart();
			}
			// Rotating
			if (!(sf::Keyboard::isKeyPressed(sf::Keyboard::K) && sf::Keyboard::isKeyPressed(sf::Keyboard::L))) {
				if (sf::Keyboard::isKeyPressed(sf::Keyboard::K) && left_rotating == false) {
					int next_anim = anim_id - 1;
					if (next_anim < 0) {
						next_anim = 3;
					}
					if (board.DoesFit(static_cast<Tetrominos::Names>(shape), next_anim, {pos.x, pos.y})) {
						anim_id = next_anim;
					}
					left_rotating = true;
				}
				if (sf::Keyboard::isKeyPressed(sf::Keyboard::L) && right_rotating == false) {
					int next_anim = anim_id + 1;
					if (next_anim > 3) {
						next_anim = 0;
					}
					if (board.DoesFit(static_cast<Tetrominos::Names>(shape), next_anim, {pos.x, pos.y})) {
						anim_id = next_anim;
					}
					right_rotating = true;
				}
			}
			if (!sf::Keyboard::isKeyPressed(sf::Keyboard::K) && left_rotating == true) {
				left_rotating = false;
			}
			if (!sf::Keyboard::isKeyPressed(sf::Keyboard::L) && right_rotating == true) {
				right_rotating = false;
			}
		}

		// Scrolling down
		if (ElapsedTime.asMilliseconds() > descending_delay) {
			if (board.DoesFit(static_cast<Tetrominos::Names>(shape), anim_id,
					{pos.x, pos.y + 1})) {
				pos.y++;
			} else {
				board.PlaceTetromino(static_cast<Tetrominos::Names>(shape), anim_id, {pos.x, pos.y});
				board.CheckLines();
				pos = {3, -2};
				anim_id = 0;
				shape = pred_shape; 
				pred_shape = static_cast<Tetrominos::Names>(std::rand() / ((RAND_MAX + 1u) / 7));
			}
			//anim_id++;
			if (anim_id >= 4) {
				anim_id = 0;
			}
			clock.restart();
		}
    sf::Event event;
    while (window.pollEvent(event))
    {
      if (event.type == sf::Event::Closed)
        window.close();
    }

		// UI
    window.clear(sf::Color(40, 40, 40, 255));
    board.Draw(window);
		window.draw(pred_box);

		// Game
		draw_tetromino(window, static_cast<Tetrominos::Names>(shape), {static_cast<float>(pos.x), static_cast<float>(pos.y)}, anim_id);
		window.draw(top_hider);
		draw_tetromino(window, static_cast<Tetrominos::Names>(pred_shape), {10.5f, 15.f}, 0);
    window.display();
  }
	return 0;
}
