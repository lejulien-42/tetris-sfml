

SRCS = ./src/main.cpp  \
			 ./src/Board.cpp \
			 ./src/Game.cpp

NAME = CelebratingTetris

FLAGS = -lsfml-graphics -lsfml-window -lsfml-system

OBJS = $(SRCS:.cpp=.o)

COMPILER = clang++

all: $(NAME)

$(NAME):	$(OBJS)
	$(COMPILER) -o $(NAME) $(FLAGS) $(OBJS)

clean:
	rm -fr $(OBJS)

fclean:		clean
	rm -fr $(NAME)

re:		fclean all

