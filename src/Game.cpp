// Celebrating tetris by Ley 2024

#include "../includes/Game.hpp"

sf::Color getTetrominoColor(const Tetrominos::Names &name) {
	switch (name) {
		case Tetrominos::Names::I:
			return(sf::Color::Cyan);
			break;
		case Tetrominos::Names::J:
			return(sf::Color::Blue);
			break;
		case Tetrominos::Names::L:
			return(sf::Color(255, 127, 0, 255));
			break;
		case Tetrominos::Names::O:
			return(sf::Color::Yellow);
			break;
		case Tetrominos::Names::S:
			return(sf::Color::Green);
			break;
		case Tetrominos::Names::T:
			return(sf::Color::Magenta);
			break;
		case Tetrominos::Names::Z:
			return(sf::Color::Red);
			break;
		case Tetrominos::Names::None:
			return(sf::Color::Black);
			break;
		default:
			break;
	};
	return sf::Color::Black;
}
