// Celebrating tetris by Ley 2024

#include "../includes/Board.hpp"

Board::Board() {
	Clear();
}

Board::~Board() {
}

void Board::Draw(sf::RenderWindow &win) {
	sf::RectangleShape board({320.f, 640.f});
	board.setOutlineThickness(4.f);
	board.setOutlineColor({});
	sf::RectangleShape cell({28.f, 28.f});
	cell.setOutlineThickness(4.f);
	cell.setOutlineColor({});

	// Draw board
  board.setFillColor(sf::Color());
	board.setPosition(16.f, 16.f);
	win.draw(board);

	// Draw cells
	for (int y = 0; y < 20; y++) {
		for (int x = 0; x < 10; x++) {
			cell.setFillColor(getTetrominoColor(static_cast<Tetrominos::Names>(buffer_[x][y])));
			cell.setPosition(x * 34.f + 16.f, y * 34.f + 16.f);
			win.draw(cell);
		}
	}
}

void Board::Clear() {
	for (int i = 0; i < 10; i++) {
		for (int j = 0; j < 20; j++) {
			buffer_[i][j] = Tetrominos::Names::None;
		}
	}
}

int Board::isInBoard(const sf::Vector2<int> pos) {
	if (pos.y >= 20) {
		return 1;
	}
	if (pos.x < 0 || pos.x >= 10) {
		return 1;
	}
	return 0;
}

int Board::DoesFit(const Tetrominos::Names name, const int rot,
		const sf::Vector2<int> pos) {
	for (int y = 0; y < 4; y++) {
		for (int x = 0; x < 4; x++) {
			if (Tetrominos::shapes[name][rot][y][x] == 1) {
				if (y + pos.y >= 0) {
					if (isInBoard({x + pos.x, y + pos.y})) {
						return 0;
					}
					if (buffer_[x + pos.x][y + pos.y] != Tetrominos::Names::None){
						return 0;
					}
				}
			}
		}
	}
	return 1;
}


void Board::PlaceTetromino(const Tetrominos::Names name, const int rot,
		const sf::Vector2<int> pos) {
	for (int y = 0; y < 4; y++) {
		for (int x = 0; x < 4; x++) {
			if (Tetrominos::shapes[name][rot][y][x] == 1) {
					buffer_[x + pos.x][y + pos.y] = name;
			}
		}
	}
}

void Board::CheckLines() {
	bool is_line;
	for (int j = 0; j < 20; j++) {
		is_line = true;
		for (int i = 0; i < 10; i++) {
			if (buffer_[i][j] == Tetrominos::Names::None) {
				is_line = false;
			}
		}
		if (is_line) {
			for (int y = j; y > 0; y--) {
				for (int x = 0; x < 10; x++) {
					buffer_[x][y] = buffer_[x][y - 1];
				}
			}
			CheckLines();
			return;
		}
	}
}
