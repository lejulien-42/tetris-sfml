# Celebrating Tetris

This is a little trial to see if I could make a tetris game within 24 hours.

Linux dependencies :
* libxrandr-dev
* libxcursor-dev
* libudev-dev
* libfreetype-dev
* libopenal-dev
* libflac-dev
* libvorbis-dev
* libgl1-mesa-dev
* libegl1-mesa-dev

## Building

```bash
mkdir build
cd build
cmake ..
make
./build/CelebratingTetris
```
